import siteinfo from '../../../../siteinfo';
// 获取系统的配置信息
Page({
  data: {
    list: [
      {
        id: 'view',
        name: 'zkdemo分包视图容器',
        open: false,
        pages: ['view', 'scroll-view']
      }
    ]
  },
  onLoad(){
  	this.reqOpenapi();
  	// console.log(siteinfo)
  	var Authorization = wx.getStorageSync('_jwtoken') // 用户鉴权token 如果是开发模式可以模拟一下,线上系统是可用的
      if((Authorization) =='' || Authorization== undefined){
        Authorization = ''
      }
  	var header = {'Content-Type': 'application/json','Authorization': Authorization};
  	var res = wx.request({
            url: siteinfo.production+'fr-test',
            method: 'POST',
            data:  {},
            header: header,
            success: function(res){
            	// {"errcode":4003,"errmsg":"访问出错","code":1}
            	//errcode 0代表成功，其他代表错误,errmsg 里面为数据或者原因 
            	console.log(res)
            	// 你的处理逻辑在此
            }
        });
  },
  // graphql 方式的请求，openapi的数据可以采用以下方式进行请求
  reqOpenapi: function(){
  	var params = {
  		 query: `
                  query goodlist($page: Int) {
                              goodlist(page: $page) {
                                mid
                                wxid
                                pic
                                minPrice
                                originalPrice
                                goodtype
                                characteristic
                                stores
                                name
                                ismultoption
                                sellnums
                              }
                            }
                `,
        variables: {
          page: 1
        }
  	}
  	 params = JSON.stringify(params)
    
     var Authorization = wx.getStorageSync('_jwtoken') // 用户鉴权token 如果是开发模式可以模拟一下,线上系统是可用的
      if((Authorization) =='' || Authorization== undefined){
        Authorization = ''
      }
      var header = {'Content-Type': 'application/json','Authorization':'Bearer '+Authorization};
  	var res = wx.request({
            url: siteinfo.production+'api-api',
            method: 'POST',
            data:  {},
            header: header,
            success: function(res){
            	// {"errcode":4003,"errmsg":"访问出错","code":1}
            	//errcode 0代表成功，其他代表错误,errmsg 里面为数据或者原因 
            	console.log(res)
            	// 你的处理逻辑在此
            }
        });
  },
  kindToggle: function (e) {
    var id = e.currentTarget.id, list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list
    });
  }
})


