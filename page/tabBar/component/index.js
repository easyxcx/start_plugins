Page({
  data: {
    list: [
      {
        id: 'view',
        name: 'zkdemo分包视图容器',
        open: false,
        pages: ['view', 'scroll-view']
      }
    ]
  },
  kindToggle: function (e) {
    var id = e.currentTarget.id, list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list
    });
  }
})

